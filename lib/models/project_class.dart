class Project {
  String title;
  String duration;
  String instaId;

  Project(this.title, this.duration, this.instaId);

  @override
  String toString() {
    return 'Project{title: $title, duration: $duration, instaId: $instaId}';
  }
}