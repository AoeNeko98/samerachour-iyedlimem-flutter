class NotificationClass{
  String title;
  String time;
  String contenu;

  NotificationClass(this.title, this.time, this.contenu);

  @override
  String toString() {
    return 'NotificationClass{title: $title, time: $time, contenu: $contenu}';
  }
}