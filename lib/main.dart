import 'package:Flenci/screens/editprofileowner.dart';
import 'package:Flenci/screens/notif_menu.dart';
import 'package:Flenci/screens/profilecompanyowner.dart';
import 'package:Flenci/screens/profileinstag.dart';
import 'package:Flenci/screens/signin.dart';
import 'package:Flenci/screens/signup.dart';
import 'package:Flenci/screens/signup_flenci.dart';
import 'package:Flenci/screens/splash_screen.dart';
import 'package:Flenci/screens/view_insta_profile.dart';
import 'package:Flenci/widgets/nav_bot/nav_bot_insta.dart';
import 'package:Flenci/widgets/nav_bot/nav_bot_owner.dart';

import 'package:flutter/material.dart';




void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flenci',
      routes: {
        "/": (BuildContext context) {
          return const SplachScreen();
        },
        "/signin": (BuildContext context) {
          return const Signin();
        },


        "/signup": (BuildContext context) {
          return const Signup();
        },
        "/signupflenci": (BuildContext context) {
          return const RegisterFlenci();
        },
        "/insta/profile": (BuildContext context) {
          return const Profile();
        },
        "/owner/profile": (BuildContext context) {
          return const ProfileOwner();
        },
        "/owner/edit": (BuildContext context) {
          return const EditProfile();
        },

        "/insta/navbot": (BuildContext context) {
          return const NavBotInsta();
        },
        "/owner/navbot": (BuildContext context) {
          return const NavBotOwner();
        },
        "/owner/notification": (BuildContext context) {
          return const NotificationScreen();
        },
        "/owner/instaprofile": (BuildContext context) {
          return const ViewInstaProfile();
        },


      },
    );
  }
}