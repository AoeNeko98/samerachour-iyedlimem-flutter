import 'package:flutter/material.dart';
class Messagerie extends StatelessWidget {
  const Messagerie({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Color(0xffe52b5d),
                  width: 2.50,
                ),
                color: Colors.white,
              ),
              child: Image.asset("assets/images/Ellipse.png"),
            ),
            Container(
              width: 123,
              height: 30,
              child: Image.asset("assets/images/logoFlenci.png"),
            ),
            Expanded(
              child: const Text(
                "Chat",
                textAlign: TextAlign.right,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontFamily: "Mada",
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),


    );
  }
}
