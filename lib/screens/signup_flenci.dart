import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';

class RegisterFlenci extends StatefulWidget {
  const RegisterFlenci({Key? key}) : super(key: key);

  @override
  State<RegisterFlenci> createState() => _RegisterFlenciState();
}

class _RegisterFlenciState extends State<RegisterFlenci> {
  late String? _name;
  late String? _email;
  late String? _password;

  final String _baseUrl = "flenci.herokuapp.com";

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  String? dropdownValue;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('Register',
            style: TextStyle(
                fontSize: 20, color: Colors.black87, fontFamily: 'Mada')),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Form(
        key: _keyForm,
        child: ListView(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(0, 20, 20, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    width: 15,
                    height: 15,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0xff8983d7),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: 40,
                    height: 15,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(22),
                      gradient: const LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Color(0xffeb2834),
                          Color(0xffca337e),
                          Color(0xffa93d8c)
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
                width: double.infinity,
                margin: const EdgeInsets.fromLTRB(20, 40, 20, 10),
                child: Image.asset("assets/images/logoFlenci.png",
                    width: 50, height: 50)),
            Container(
              margin: const EdgeInsets.fromLTRB(40, 50, 40, 10),
              child: TextFormField(
                decoration: const InputDecoration(
                    suffixIcon: Icon(
                      Icons.account_circle,
                      color: Colors.black,
                    ),
                    border: UnderlineInputBorder(),
                    labelText: "Company name"),
                onSaved: (String? value) {
                  _name = value;
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(40, 0, 40, 10),
              width: 306,
              height: 64,
              child: DropdownButton<String>(
                value: dropdownValue,
                hint: Text('Field of business'),
                icon: const Icon(Icons.arrow_drop_down),
                isExpanded: true,
                onChanged: (String? newValue) {
                  setState(() {
                    dropdownValue = newValue!;
                  });
                },
                items: <String>['Entertainment', 'Food', 'health and Style']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(40, 0, 40, 10),
              child: TextFormField(
                decoration: const InputDecoration(
                    suffixIcon: Icon(
                      Icons.mail,
                      color: Colors.black,
                    ),
                    border: UnderlineInputBorder(),
                    labelText: "Email"),
                onSaved: (String? value) {
                  _email = value;
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(40, 0, 40, 10),
              child: TextFormField(
                obscureText: true,
                decoration: const InputDecoration(
                    suffixIcon: Icon(
                      Icons.visibility_off_outlined,
                      color: Colors.black,
                    ),
                    border: UnderlineInputBorder(),
                    labelText: "Password"),
                onSaved: (String? value) {
                  _password = value;
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(50, 100, 50, 0),
              height: 50.0,
              width: 100.0,
              //width: double.infinity,

              child: ElevatedButton(
                onPressed: () {
                  if (_keyForm.currentState!.validate()) {
                    _keyForm.currentState!.save();
                    Map<String, dynamic> userData = {
                      "name": _name,
                      "email": _email,
                      "pwd": _password,
                      "field": dropdownValue,

                    };
                    Map<String, String> headers = {
                      "Content-Type": "application/json; charset=UTF-8"
                    };
                    http
                        .post(Uri.https(_baseUrl, "/api/user/register"),
                        headers: headers, body: json.encode(userData)).then((http.Response response){
                      if (response.statusCode == 200) {
                        Navigator.pushReplacementNamed(context, "/signin");
                      } else if(response.statusCode == 400) {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return const AlertDialog(
                                title: Text("Information"),
                                content: Text("User Already exist"),
                              );
                            });
                      }else  {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return  AlertDialog(
                                title: Text("Information"),
                                content: Text("Erreur"),
                              );
                            });
                      }
                    });

                  }

                },
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                    )),
                    elevation: MaterialStateProperty.all(0.0),
                    backgroundColor: MaterialStateProperty.all(Colors.white)),
                child: Ink(
                  decoration: BoxDecoration(
                      gradient: const LinearGradient(
                        colors: [
                          Color(0xffEB2834),
                          Color(0xffCA337E),
                          Color(0xffA93D8C)
                        ],
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                      ),
                      borderRadius: BorderRadius.circular(30.0)),
                  child: Container(
                    // ink width to fit-parent
                    constraints: const BoxConstraints(
                        minWidth: double.infinity, minHeight: 50.0),
                    alignment: Alignment.center,
                    child: const Text(
                      "Confirm",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Mada',
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("You already have an account ?"),
                  const SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                    child: const Text("Login",
                        style: TextStyle(
                            color: Color(0xffE42B46),
                            fontFamily: 'Mada',
                            fontWeight: FontWeight.bold)),
                    onTap: () {
                      Navigator.pushReplacementNamed(context, "/signin");
                      //   Navigator.pushNamed(context, "/resetPwd");,
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
