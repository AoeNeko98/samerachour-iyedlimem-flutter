import 'package:Flenci/models/notification_class.dart';
import 'package:Flenci/widgets/notif_card.dart';
import 'package:flutter/material.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  final List<NotificationClass> _notification = [];

  @override
  void initState() {
    for (int i = 0; i < 25; i++) {
      _notification.add(NotificationClass("iyed Limem", "13:10", "you have a partnership request from Iyed Limem with budget 25000dt about sports"));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
            ),
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Color(0xffe52b5d),
                    width: 2.50,
                  ),
                  color: Colors.white,
                ),
                child: Image.asset("assets/images/Ellipse.png"),
              ),
              Container(
                width: 123,
                height: 30,
                child: Image.asset("assets/images/logoFlenci.png"),
              ),
              Expanded(
                child: const Text(
                  "Notification",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: "Mada",
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),

        body: ListView(
          children: [
            Center(
              child: Container(
                 
                  height: 600,
                  margin:EdgeInsets.fromLTRB(0, 20, 0, 10),
                  color: Colors.white,
                  child: RawScrollbar(
                    thumbColor: const Color(0xfff4abbb),
                    radius: const Radius.circular(20),
                    child: Scrollbar(
                      thickness: 0,
                      child: ListView.builder(
                        itemCount: _notification.length,
                        itemBuilder: (BuildContext context, int index) {
                          return NotifCard(
                              _notification[index].title,
                              _notification[index].time,
                              _notification[index].contenu);
                        },
                      ),
                    ),
                  )),
            )
          ],
        )
    );
  }
}
