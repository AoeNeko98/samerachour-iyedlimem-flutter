import 'package:Flenci/models/project_class.dart';
import 'package:Flenci/widgets/project_inprogress.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileOwner extends StatefulWidget {
  const ProfileOwner({Key? key}) : super(key: key);

  @override
  State<ProfileOwner> createState() => _ProfileOwnerState();
}

class _ProfileOwnerState extends State<ProfileOwner> {
  final List<Project> _goingProjects = [];
  final String _baseUrl = "flenci.herokuapp.com";
  late String? _userId;
  late String? name;
  late String? field;
  late String? email;
  late String? img;


  late Future<bool> fetchedUser;
  Future<bool> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _userId = prefs.getString("userId");
    http.Response response =
        await http.get(Uri.http(_baseUrl, "/api/user/" + _userId!));
    dynamic userFromServer = json.decode(response.body);
    print(userFromServer);
    name = userFromServer["name"];
    field = userFromServer["field"];
    email = userFromServer["email"];
    img = userFromServer["img"];
    for (int i = 0; i < userFromServer["requests"].length; i++) {
      print(userFromServer["requests"][i]["_id"]);
      _goingProjects.add(Project(
          userFromServer["requests"][i]["title"],
          userFromServer["requests"][i]["duration"],
          userFromServer["requests"][i]["insta"]["username"]));
    }

    return true;
  }

  @override
  void initState() {
    fetchedUser = getUser();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchedUser,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
     return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pushReplacementNamed(context, "/owner/navbot");
          },
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage("https://flenci.herokuapp.com/"+img!)),
                shape: BoxShape.circle,
                border: Border.all(
                  color: Color(0xffe52b5d),
                  width: 2.50,
                ),
                color: Colors.white,
              ),
            ),
            Container(
              width: 123,
              height: 30,
              child: Image.asset("assets/images/logoFlenci.png"),
            ),
            Expanded(
              child: const Text(
                "Profil",
                textAlign: TextAlign.right,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontFamily: "Mada",
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),
      body: ListView(
              children: [
                Container(
                  width: 110,
                  height: 110,
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Color(0xffe52b5d),
                      width: 2.50,
                    ),
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Container(
                      width: 96,
                      height: 92,
                      decoration:  BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage("https://flenci.herokuapp.com/"+img!),
                        fit: BoxFit.cover),
                        shape: BoxShape.circle,
                      ),

                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Text(
                    name!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff363636),
                      fontSize: 22,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Text(
                    field!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                ),
                Text(
                  email!,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(60, 20, 60, 20),
                  child: Divider(
                    height: 2,
                    color: Color(0xff5e5e5e),
                  ),
                ),
                Center(
                  child: Column(
                    children: [
                      Container(
                        width: 37,
                        height: 37,
                        margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(11),
                          color: Color(0xffdfdada),
                        ),
                        child: Icon(Icons.apps),
                      ),
                      const Text(
                        "Project In Progress",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
                Center(
                  child: Container(
                      width: 348,
                      height: 300,
                      margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),

                      child: RawScrollbar(
                        thumbColor: const Color(0xfff4abbb),
                        radius: const Radius.circular(20),
                        child: Scrollbar(
                          thickness: 0,
                          child: ListView.builder(
                            itemCount: _goingProjects.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ProjectInProgress(
                                _goingProjects[index].title,
                                _goingProjects[index].duration,
                                _goingProjects[index].instaId,
                                img!
                              );
                            },
                          ),
                        ),
                      )),
                )
              ],
            ));
          } else if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          } else {
            return const Center(
              child: const CircularProgressIndicator(),
            );
          }
        },
      );

  }
}
