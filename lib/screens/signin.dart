import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class Signin extends StatefulWidget {
  const Signin({Key? key}) : super(key: key);

  @override
  State<Signin> createState() => _SigninState();
}

class _SigninState extends State<Signin> {
  late String? _email;
  late String? _password;

  final String _baseUrl = "flenci.herokuapp.com";
  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('Login',
            style: TextStyle(
                fontSize: 20, color: Colors.black87, fontFamily: 'Mada')),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Form(
        key: _keyForm,
        child: ListView(
          children: [
            Container(
                width: double.infinity,
                margin: const EdgeInsets.fromLTRB(20, 40, 20, 10),
                child: Image.asset("assets/images/logoFlenci.png",
                    width: 50, height: 50)),
            Container(
              margin: const EdgeInsets.fromLTRB(40, 50, 40, 10),
              child: TextFormField(
                decoration: const InputDecoration(
                    suffixIcon: Icon(
                      Icons.account_circle,
                      color: Colors.black,
                    ),
                    border: UnderlineInputBorder(),
                    labelText: "Username ou email"),
                onSaved: (String? value) {
                  _email = value;
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(40, 0, 40, 10),
              child: TextFormField(
                obscureText: true,
                decoration: const InputDecoration(
                    suffixIcon: Icon(
                      Icons.remove_red_eye_sharp,
                      color: Colors.black,
                    ),
                    border: UnderlineInputBorder(),
                    labelText: "Mot de passe"),
                onSaved: (String? value) {
                  _password = value;
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(40, 10, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    child: const Text("forget your password ?",
                        style: TextStyle(
                            color: Color(0xffE42B46),
                            fontFamily: 'Mada',
                            fontSize: 12)),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return BackdropFilter(
                              child: AlertDialog(
                                title: const Text(
                                  'Reset Password',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 22,
                                    fontFamily: "Mada",
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                content: Container(
                                  height: 120,
                                  child: Column(
                                    children: [
                                      const TextField(
                                        decoration: InputDecoration(
                                            suffixIcon: Icon(
                                              Icons.mail,
                                              color: Colors.black,
                                            ),
                                            border: UnderlineInputBorder(),
                                            labelText: "Email"),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                                elevation: 10,
                                shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(32.0))),
                              ),
                              filter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
                            );
                          });
                    },
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(50, 120, 50, 0),
              height: 50.0,
              width: 100.0,
              //width: double.infinity,

              child: ElevatedButton(
                onPressed: () {
                  if (_keyForm.currentState!.validate()) {
                    _keyForm.currentState!.save();
                    Map<String, dynamic> userData = {
                      "username": _email,
                      "pwd": _password,
                    };
                    Map<String, String> headers = {
                      "Content-Type": "application/json; charset=UTF-8"
                    };
                    http
                        .post(Uri.https(_baseUrl, "/api/user/login"),
                            headers: headers, body: json.encode(userData))
                        .then((http.Response response) async {
                      if (response.statusCode == 200) {
                        Map<String, dynamic> userFromServer =
                            json.decode(response.body);
                        print(userFromServer);
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        prefs.setString(
                            "userId", userFromServer["userCheck"]["_id"]);
                        prefs.setString(
                            "role", "company");
                        prefs.setString(
                            "userName", userFromServer["userCheck"]["name"]);
                        prefs.setString(
                            "userImg", userFromServer["userCheck"]["img"]);

                        Navigator.popAndPushNamed(context, "/owner/navbot");
                      } else if (response.statusCode == 404) {
                        http
                            .post(Uri.https(_baseUrl, "/api/insta/login"),
                                headers: headers, body: json.encode(userData))
                            .then((http.Response response) async {
                          if (response.statusCode == 200) {
                            Map<String, dynamic> userFromServer =
                                json.decode(response.body);
                            print(userFromServer);
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            prefs.setString(
                                "userId", userFromServer["userCheck"]["_id"]);


                            Navigator.popAndPushNamed(context, "/insta/navbot");
                          } else if (response.statusCode == 400) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return const AlertDialog(
                                    title: Text("Information"),
                                    content: Text("Wrong password "),
                                  );
                                });
                          } else if (response.statusCode == 404) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return const AlertDialog(
                                    title: Text("Information"),
                                    content: Text("User not found "),
                                  );
                                });
                          } else {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Information"),
                                    content: Text("Erruer"),
                                  );
                                });
                          }
                        });
                      } else if (response.statusCode == 400) {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return const AlertDialog(
                                title: Text("Information"),
                                content: Text("Wrong password "),
                              );
                            });
                      } else {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Information"),
                                content: Text("Erruer"),
                              );
                            });
                      }
                    });
                  }
                },
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                    )),
                    elevation: MaterialStateProperty.all(0.0),
                    backgroundColor: MaterialStateProperty.all(Colors.white)),
                child: Ink(
                  decoration: BoxDecoration(
                      gradient: const LinearGradient(
                        colors: [
                          Color(0xffEB2834),
                          Color(0xffCA337E),
                          Color(0xffA93D8C)
                        ],
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                      ),
                      borderRadius: BorderRadius.circular(30.0)),
                  child: Container(
                    // ink width to fit-parent
                    constraints: const BoxConstraints(
                        minWidth: double.infinity, minHeight: 50.0),
                    alignment: Alignment.center,
                    child: const Text(
                      "Confirm",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Mada',
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("You dont have an account yet ?"),
                  const SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                    child: const Text("try to Register",
                        style: TextStyle(
                            color: Color(0xffE42B46),
                            fontFamily: 'Mada',
                            fontWeight: FontWeight.bold)),
                    onTap: () {
                      Navigator.pushReplacementNamed(context, "/signup");
                      //   Navigator.pushNamed(context, "/resetPwd");,
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
