import 'package:Flenci/widgets/profile_value.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
   bool _reach = true;
   bool _pub = true;
   bool _impression = true;
   bool _views = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(

            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,

              children: [
                SizedBox (height: 50),
                ListTile(
                  title: Row(
                    children: [
                      Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage("assets/images/userCompany.png")
                          ),
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Color(0xffe52b5d),
                            width: 2.50,
                          ),
                          color: Colors.white,
                        ),

                      ),
                      SizedBox(width: 10),
                      const Text('Semer Achour',style: TextStyle(fontSize: 20),),
                    ],
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, "/");
                  },
                ),
                ListTile(
                    title:

                    Divider(height: 000,color: Colors.black,thickness: 2,)


                ),
                ListTile(
                  title: Row(
                    children: [
                      const Text('Profil Insights'),
                      SizedBox ( width: 6,),
                      Icon(Icons.insights),
                    ],
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, "/owner/instaprofile");
                  },
                ),
                ListTile(
                  title: Row(
                    children: [
                      const Text('Edit Profil'),
                      SizedBox ( width: 6,),
                      Icon(Icons.edit),
                    ],
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, "/owner/edit");
                  },
                ),
                ListTile(
                  title: Row(
                    children: [
                      const Text('Sign Out'),
                      SizedBox ( width: 6,),
                      Icon(Icons.logout),
                    ],
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, "/signin");
                  },
                ),

                ListTile(
                  title: Row(
                    children: [
                      const Text('Delete Account'),
                      SizedBox ( width: 6,),
                      Icon(Icons.delete),
                    ],
                  ),
                  onTap: () {},
                ),
              ],
            )),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
            ),
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage("assets/images/influ.jpg")),
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Color(0xffe52b5d),
                    width: 2.50,
                  ),
                  color: Colors.white,
                ),
              ),
              Container(
                width: 123,
                height: 30,
                child: Image.asset("assets/images/logoFlenci.png"),
              ),
              Expanded(
                child: const Text(
                  "Profil",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: "Mada",
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        body: ListView(children: [
          Container(
            width: 110,
            height: 110,
            margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: Color(0xffe52b5d),
                width: 2.50,
              ),
              color: Colors.white,
            ),
            child: Center(
              child: Container(
                width: 96,
                height: 92,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/influ.jpg"),
                      fit: BoxFit.cover),
                  shape: BoxShape.circle,
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
            child: const Text(
              "Comany Name",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xff363636),
                fontSize: 22,
                fontFamily: "Inter",
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            width: 167,
            height: 33,
            child: Text(
              "Field of Buisness",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Container(
                width: 89,
                height: 35,
                child: Text("52.6k\nFollowers",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    ))),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(60, 20, 60, 20),
            child: Divider(
              height: 2,
              color: Color(0xff5e5e5e),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                      child: _reach?
                      Container(
                          width: 115,
                          height: 115,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x0c000000),
                                blurRadius: 25,
                                offset: Offset(0, 10),
                              ),
                            ],
                            color: Colors.white,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.bar_chart,
                                color: Color(0xffe187b3),
                                size: 75,
                              ),
                              Text(
                                "Reach",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          )):
                      ProfileValue("128","Reach"),
                      onTap:()async{
                          setState(() {
                            _reach =!_reach;
                          });

                      },
                    ),
                    InkWell(
                      child: _pub?
                      Container(
                        width: 115,
                        height: 115,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0x0c000000),
                              blurRadius: 25,
                              offset: Offset(0, 10),
                            ),
                          ],
                          color: Colors.white,
                        ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.forum,
                            color: Color(0xffe187b3),
                            size: 75,
                          ),
                          Text(
                            "Publications",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      ):
                      ProfileValue("140","Publications"),
                      onTap: ()async{
                        setState(() {
                          _pub=!_pub;
                        });
                      },
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                      child: _impression?
                      Container(
                        width: 115,
                        height: 115,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0x0c000000),
                              blurRadius: 25,
                              offset: Offset(0, 10),
                            ),
                          ],
                          color: Colors.white,
                        ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.favorite_border,
                                color: Color(0xffe187b3),
                                size: 75,
                              ),
                              Text(
                                "Impressions",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          )
                      ):
                      ProfileValue("5K","Impressions"),
                      onTap: ()async{
                        setState(() {
                          _impression=!_impression;
                        });
                      },
                    ),
                    InkWell(
                      child: _views?
                      Container(
                        width: 115,
                        height: 115,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0x0c000000),
                              blurRadius: 25,
                              offset: Offset(0, 10),
                            ),
                          ],
                          color: Colors.white,
                        ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.people,
                                color: Color(0xffe187b3),
                                size: 75,
                              ),
                              Text(
                                "Views",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          )
                      ):
                      ProfileValue("8K","Views"),
                      onTap: ()async{
                        setState(() {
                          _views=!_views;
                        });
                      },
                    ),
                  ],
                ),

              ],
            ),
          )
        ]));
  }
}
