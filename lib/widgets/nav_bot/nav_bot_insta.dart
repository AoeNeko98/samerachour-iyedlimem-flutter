import 'package:Flenci/screens/board.dart';
import 'package:Flenci/screens/messagerie.dart';
import 'package:Flenci/screens/notif_menu.dart';
import 'package:Flenci/screens/profileinstag.dart';
import 'package:flutter/material.dart';



class NavBotInsta extends StatefulWidget {
  const NavBotInsta({Key? key}) : super(key: key);

  @override
  _NavBotInstaState createState() => _NavBotInstaState();
}

class _NavBotInstaState extends State<NavBotInsta> {
  int _currentIndex = 0;
  final List<Widget> _interfaces = [
    const NotificationScreen(),
    const Board(),
    const Messagerie(),


  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _interfaces[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Color(0xffcb337d),
        items: const [
          BottomNavigationBarItem(label: "Notification", icon: Icon(Icons.notifications)),
          BottomNavigationBarItem(
              label: "Board", icon:Icon(
            Icons.assignment_outlined,
          ),),
          BottomNavigationBarItem(label: "Chat", icon: Icon(Icons.message)),

        ],
        currentIndex: _currentIndex,
        onTap: (int value) {
          setState(() {
            _currentIndex = value;
          });
        },
      ),
    );
  }
}
