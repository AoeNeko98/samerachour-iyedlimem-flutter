import 'dart:ui';
import 'package:http/http.dart'as http;
import 'package:Flenci/models/instagrameuse_class.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class MenuCard extends StatefulWidget {

  final String _username;
  final String _id;

  const MenuCard( this._username,this._id,
      {Key? key})
      : super(key: key);

  @override
  State<MenuCard> createState() => _MenuCardState();
}

class _MenuCardState extends State<MenuCard> {

  final List<Instagrameuse> _instagrameuseList=[];
  final String _baseUrl = "flenci.herokuapp.com";
  late Future<bool> fetchedInfluencers ;
  Future<bool> getInfluencers()async{
    http.Response response = await http.get(Uri.http(_baseUrl, "/api/insta/"+widget._username));

dynamic influfromServer = json.decode(response.body);

     _instagrameuseList.add(Instagrameuse(
       influfromServer["profile_picture_url"],
       influfromServer["name"],
       influfromServer["username"],
       influfromServer["biography"],
       int.parse(influfromServer["followers_count"].toString()),
       influfromServer["engagement_rate"],
       influfromServer["average_likes"],
       int.parse(influfromServer["media_count"].toString()),
         influfromServer["average_comments"]
     ));

    return true;
  }
  @override
  void initState() {
fetchedInfluencers=getInfluencers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchedInfluencers,
        builder: (BuildContext context , AsyncSnapshot snapshot){
          if(snapshot.hasData){
            return Container(
              width: 321,
              height: 76,
              margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x0c000000),
                    blurRadius: 25,
                    offset: Offset(0, 10),
                  ),
                ],
                color: Colors.white,
              ),
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 55,
                      height: 55,
                      margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(_instagrameuseList[0].image),
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(12),
                      ),

                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                      children: [
                        Row(

                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                              child: Text(
                                _instagrameuseList[0].name,
                                style: TextStyle(
                                  color: Color(0xff363636),
                                  fontSize: 11,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            Text(
                              widget._username,
                              style: TextStyle(
                                color: Color(0xff666666),
                                fontSize: 9,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          _instagrameuseList[0].intersets,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 9,
                            fontFamily: "Inter",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          _instagrameuseList[0].followers.toString()+" followers",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 9,
                            fontFamily: "Inter",
                            fontWeight: FontWeight.w500,
                          ),
                        ),

                      ],
                    ),
                    Spacer(),
                    InkWell(
                      child: Container(
                        width: 30,
                        height: 30,

                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(11),
                          color: Color(0xfffbd9ea),
                        ),
                        child: Icon(Icons.visibility,color: Color(0xffc7357c),),
                      ),
                      onTap: ()async{
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        prefs.setString("influid",  widget._id);
                        prefs.setString("influimg",  _instagrameuseList[0].image);
                        prefs.setString("influname",  _instagrameuseList[0].name);
                        prefs.setString("influbio",  _instagrameuseList[0].intersets);
                        prefs.setString("influusername",  _instagrameuseList[0].username);
                        prefs.setString("influcomment",  _instagrameuseList[0].comments);
                        prefs.setString("influengagment",  _instagrameuseList[0].engagment);
                        prefs.setString("influimpresseon",  _instagrameuseList[0].impression);
                        prefs.setInt("influfollowers",  _instagrameuseList[0].followers);
                        prefs.setInt("influposts",  _instagrameuseList[0].posts);

                        Navigator.pushNamed(context,  "/owner/instaprofile");
                      },
                    )
                  ],
                ),
              ),
            );
          }else if(snapshot.hasError){
            return Text(snapshot.error.toString());
          }
          else{
            return Container(
              width: 321,
              height: 76,
              margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x0c000000),
                    blurRadius: 25,
                    offset: Offset(0, 10),
                  ),
                ],
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 55,
                    height: 55,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/influ.jpg"),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),

                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(

                        children: [
                          Container(
                            margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                            child: Text(
                              "--",
                              style: TextStyle(
                                color: Color(0xff363636),
                                fontSize: 11,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          Text(
                            widget._username,
                            style: TextStyle(
                              color: Color(0xff666666),
                              fontSize: 9,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        "--",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 9,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        "--".toString()+"K followers",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 9,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w500,
                        ),
                      ),

                    ],
                  ),
                  InkWell(
                    child: Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(11),
                        color: Color(0xfffbd9ea),
                      ),
                      child: Icon(Icons.visibility,color: Color(0xffc7357c),),
                    ),
                    onTap: (){

                      Navigator.pushNamed(context,  "/owner/instaprofile");
                    },
                  )
                ],
              ),
            );
          }
    });
  }
}
